<?php

Route::get('/', function() {
	echo "Chào";
});
Route::get('quan-tri', function () {
    return view('Backend.Layout.layout');
});

Auth::routes();

Route::group(['prefix' => 'act-quan-tri/' , 'middleware' => 'auth'], function () {
	Route::get('/','Backend\HomeController@index');
	Route::get('/act-user','Backend\UserController@index');
});
