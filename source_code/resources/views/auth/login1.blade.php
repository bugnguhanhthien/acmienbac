<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="img/favicon.png">

    <title>ACT Miền Bắc - Quản Trị Website</title>

    <!-- Bootstrap CSS -->    
    <link href="{{ URL::asset('Backend/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- bootstrap theme -->
    <link href="{{ URL::asset('Backend/css/bootstrap-theme.css')}}" rel="stylesheet">
    <!--external css-->
    <!-- font icon -->
    <link href="{{ URL::asset('Backend/css/elegant-icons-style.css')}}" rel="stylesheet" />
    <link href="{{ URL::asset('Backend/css/font-awesome.css')}}" rel="stylesheet" />
    <!-- Custom styles -->
    <link href="{{ URL::asset('Backend/css/style.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('Backend/css/style-responsive.css')}}" rel="stylesheet" />
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>   
</head>

  <body class="login-img3-body">

    <div class="container">

      <form class="login-form" method="POST" action="{{ url('/act-login') }}"> 
        {{ csrf_field() }}       
        <div class="login-wrap">
            <p class="login-img"><i class="icon_lock_alt"></i></p>
            <h3 style="text-align: center;">Đăng nhập hệ thống quản trị</h3>
            <div class="input-group">
                <span class="input-group-addon"><i class="icon_profile"></i></span>
                <input type="text" class="form-control" placeholder="Email đăng nhập" name="email" value="{{ old('email') }}" autofocus>
            </div>
            @if ($errors->has('email'))
                <div class="{{ $errors->has('email') ? ' has-error' : '' }}">
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                </div>
            @endif
            <div class="input-group {{ $errors->has('password') ? ' has-error' : '' }}">
                <span class="input-group-addon"><i class="icon_key_alt"></i></span>
                <input type="password" class="form-control" placeholder="Mật khẩu"  type="password" class="form-control" name="password" >
            </div>
            @if ($errors->has('password'))
                <div class="{{ $errors->has('email') ? ' has-error' : '' }}">
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                </div>
            @endif
            <label class="checkbox">
                <input type="checkbox" value="remember"> Ghi nhớ
                <span class="pull-right"> <a href="{{ url('/password/reset') }}"> Lấy lại mật khẩu?</a></span>
            </label>
            <button class="btn btn-primary btn-lg btn-block" type="submit">Đăng nhập</button>
        </div>
      </form>

    </div>


  </body>
</html>
