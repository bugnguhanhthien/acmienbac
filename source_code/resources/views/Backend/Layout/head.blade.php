    <link rel="shortcut icon" href="{{ URL::asset('Backend/img/favicon.ico')}}" />
    <link href="{{ URL::asset('Backend/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- bootstrap theme -->
    <link href="{{ URL::asset('Backend/css/bootstrap-theme.css')}}" rel="stylesheet">
    <!--external css-->
    <!-- font icon -->
    <link href="{{ URL::asset('Backend/css/elegant-icons-style.css')}}" rel="stylesheet" />
    <link href="{{ URL::asset('Backend/css/font-awesome.min.css')}}" rel="stylesheet" />    
    <!-- full calendar css-->
    <link href="{{ URL::asset('Backend/assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css')}}" rel="stylesheet" />
	<link href="{{ URL::asset('Backend/assets/fullcalendar/fullcalendar/fullcalendar.css')}}" rel="stylesheet" />
    <!-- easy pie chart-->
    <link href="{{ URL::asset('Backend/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css')}}" rel="stylesheet" type="text/css" media="screen"/>
    <!-- owl carousel -->
    <link rel="stylesheet" href="{{ URL::asset('Backend/css/owl.carousel.css')}}" type="text/css">
	<link href="{{ URL::asset('Backend/css/jquery-jvectormap-1.2.2.css')}}" rel="stylesheet">
    <!-- Custom styles -->
	<link rel="stylesheet" href="{{ URL::asset('Backend/css/fullcalendar.css')}}">
	<link href="{{ URL::asset('Backend/css/widgets.css')}}" rel="stylesheet">
    <link href="{{ URL::asset('Backend/css/style.css')}}" rel="stylesheet">
    <link href="{{ URL::asset('Backend/css/style-responsive.css')}}" rel="stylesheet" />
	<link href="{{ URL::asset('Backend/css/xcharts.min.css')}}" rel=" stylesheet">	
	<link href="{{ URL::asset('Backend/css/jquery-ui-1.10.4.min.css')}}" rel="stylesheet">